﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

namespace JsonKurimas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Random r = new Random();
            string directory = AppDomain.CurrentDomain.BaseDirectory + "../../AsmensDuomenys/";

            Asmuo asmuo = new Asmuo()
            {
                Vardas = "Jonas",
                Pavarde = "Jonaitis",
                Amzius = 38,
                Atlyginimas = new List<Random>(),
                _Adresas = new Asmuo.Adresas()
                {
                    Miestas = "Vilnius",
                    Gatve = "Laisves pr.",
                    Namas = 17,
                }
            };

            

            var json = JsonConvert.SerializeObject(asmuo);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            var filename = $"{asmuo.Vardas}_{asmuo.Pavarde}_duomenys.json";

            File.WriteAllText(directory + filename, json);
        }
    }

    public class Asmuo
    {
        public string Vardas { get; set; }

        public string Pavarde { get; set; }

        public int Amzius { get; set; }

        public List<Random> Atlyginimas { get; set; }

        public Adresas _Adresas { get; set; }

        public class Adresas
        {
            public string Miestas { get; set; }

            public string Gatve { get; set; }

            public int Namas { get; set; }
        }
    }
}
